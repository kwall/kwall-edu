<?php

/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

use Drupal\kwall_edu\ContentImport;

/**
 * Implements hook_install_tasks().
 */
function kwall_edu_install_tasks($install_state){
  $tasks = array(
    'kwall_edu_import_file' => array(
      'type' => 'normal'
    ),
    'kwall_edu_import_taxonomy' => array(
      'type' => 'normal'
    ),
    'kwall_edu_import_media' => array(
      'type' => 'normal'
    ),
    'kwall_edu_import_paragraph' => array(
      'type' => 'normal'
    ),
    'kwall_edu_import_block' => array(
      'type' => 'normal'
    ),
    'kwall_edu_import_node' => array(
      'type' => 'normal'
    ),
    'kwall_edu_import_menu' => array(
      'type' => 'normal'
    )
  );

  return $tasks;
}


/**
 * Import files.
 */
function kwall_edu_import_file() {
  \Drupal::classResolver()->getInstanceFromDefinition(ContentImport::class)->importFile();
  \Drupal::logger('kwall_edu')->notice('Files queue created');
}

/**
 * Import tsxonomy terms.
 */
function kwall_edu_import_taxonomy() {
  \Drupal::classResolver()->getInstanceFromDefinition(ContentImport::class)->importTaxonomy();
  \Drupal::logger('kwall_edu')->notice('Taxonomy queue created');
}

/**
 * Import media.
 */
function kwall_edu_import_media() {
  \Drupal::classResolver()->getInstanceFromDefinition(ContentImport::class)->importMedia();
  \Drupal::logger('kwall_edu')->notice('Media queue created');
}

/**
 * Import paragraphs.
 */
function kwall_edu_import_paragraph() {
  \Drupal::classResolver()->getInstanceFromDefinition(ContentImport::class)->importEntityContent('paragraph');
  \Drupal::logger('kwall_edu')->notice('Paragraph queue created');
}

/**
 * Import custom block content.
 */
function kwall_edu_import_block() {
  \Drupal::classResolver()->getInstanceFromDefinition(ContentImport::class)->importEntityContent('block_content');
  \Drupal::logger('kwall_edu')->notice('Block queue created');
}

/**
 * Import nodes.
 */
function kwall_edu_import_node() {
  \Drupal::classResolver()->getInstanceFromDefinition(ContentImport::class)->importEntityContent('node');
  \Drupal::logger('kwall_edu')->notice('Node queue created');
}

/**
 * Import menu links.
 */
function kwall_edu_import_menu() {
  \Drupal::classResolver()->getInstanceFromDefinition(ContentImport::class)->importMenuLinks();
  \Drupal::logger('kwall_edu')->notice('Menu queue created');
}

/**
 * Import menu links.
 */
function kwall_edu_update_block() {
  \Drupal::classResolver()->getInstanceFromDefinition(ContentImport::class)->updateBlock();
  \Drupal::logger('kwall_edu')->notice('Blocks updates');
}
