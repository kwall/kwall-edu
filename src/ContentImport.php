<?php

namespace Drupal\kwall_edu;

use DateTimeZone;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a helper class for importing default content.
 */
class ContentImport implements ContainerInjectionInterface
{

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new InstallHelper object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
    );
  }

  /**
   * Imports taxonomy terms.
   */
  public function importTaxonomy() {
    $json = file_get_contents(\Drupal::service('extension.list.profile')->getPath('kwall_edu') . '/content-export/taxonomy.json');
    $data = json_decode($json, TRUE);

    $queue = \Drupal::queue('kwall_edu_import_taxonomy_term');
    $queue->createQueue();

    foreach ($data as $item) {
      $vid = $item['vid'][0]['target_id'];
      $values = [
        'tid' => $item['tid'][0]['value'],
        'name' => $item['name'][0]['value'],
        'vid' => $vid,
      ];

      switch ($vid) {
        case 'interest':
          if (!empty($item['field_interest_icon'][0])) {
            unset($item['field_interest_icon'][0]['settings']);
            $values['field_interest_icon'] = $item['field_interest_icon'];
          }

          break;

        case 'degree_type':
          if (!empty($item["field_acronym"][0])) {
            $values['field_acronym'] = $item["field_acronym"][0]["value"];
          }
          if (!empty($item["field_degree_type_icon"][0])) {
            unset($item["field_degree_type_icon"][0]['settings']);
            $values['field_degree_type_icon'] = $item['field_degree_type_icon'];
          }
          if (!empty($item["field_use_acronym"][0])) {
            $values['field_use_acronym'] = $item["field_use_acronym"][0]["value"];
          }
          break;

        case 'other_degree_options':
          if (!empty($item["field_degree_type_icon"][0])) {
            unset($item["field_degree_type_icon"][0]['settings']);
            $values['field_degree_type_icon'] = $item['field_degree_type_icon'];
          }
          break;
      }

      $queue->createItem($values);
    }
  }

  /**
   * Imports files.
   */
  public function importFile() {
    $json = file_get_contents(\Drupal::service('extension.list.profile')->getPath('kwall_edu') . '/content-export/file.json');
    $data = json_decode($json, TRUE);

    $queue = \Drupal::queue('kwall_edu_import_file');
    $queue->createQueue();

    foreach ($data as $item) {
      $url = explode('sites/default/files', $item['uri'][0]["url"])[1];
      if (!empty($url)) {
        $source = \Drupal::service('extension.list.profile')->getPath('kwall_edu') . '/images' . $url;
        if (file_exists($source)) {
          $destination = 'public://' . $item["filename"][0]["value"];
          $this->fileSystem->copy($source, $destination, FileSystemInterface::EXISTS_REPLACE);
          $item['uri'][0]['value'] = $destination;
          $queue->createItem($item);
        }
      }
    }
  }

  /**
   * Imports media.
   */
  public function importMedia() {
    $json = file_get_contents(\Drupal::service('extension.list.profile')->getPath('kwall_edu') . '/content-export/media.json');
    $data = json_decode($json, TRUE);

    $queue = \Drupal::queue('kwall_edu_import_media');
    $queue->createQueue();

    $mediaField = [
      'article_featured_image' => 'field_media_image_9',
      'audio' => 'field_media_audio_file',
      'column_cta_image' => 'field_media_image_3',
      'course_image' => 'field_media_image_13',
      'document' => 'field_media_file',
      'event_featured_image' => 'field_media_image_8',
      'file' => 'field_media_file',
      'full_width_image_section' => 'field_media_image_5',
      'gridder_icon' => 'field_media_image_12',
      'hero_banner_image' => 'field_media_image_1',
      'image' => 'field_media_image',
      'inline_media' => 'field_media_image_10',
      'interior_slideshow_slide' => 'field_media_image_4',
      'person_profile_image' => 'field_media_image_6',
      'portrait' => 'field_media_image_2',
      'remote_video' => 'field_media_oembed_video',
      'story_image' => 'field_media_image_17',
      'video' => 'field_media_video_file'
    ];

    foreach ($data as $item) {

      $bundle = $item["bundle"][0]["target_id"];
      $field = $mediaField[$bundle];

      if ($field == 'field_media_oembed_video') {
        $fieldData = [
          'value' => $item[$field][0]["value"]
        ];
      } else {
        $fieldData = $item[$field][0];
      }

      $queue->createItem([
        'mid' => $item["mid"][0]["value"],
        'bundle' => $bundle,
        'uid' => 1,
        'name' => $item["name"][0]["value"],
        'status' => 1,
        $field => $fieldData,
      ]);
    }

  }

  /**
   * Imports entity content.
   */
  public function importEntityContent($entityType) {

    $entityFieldTypes = $this->getEntityFieldDefinition($entityType);

    $json = file_get_contents(\Drupal::service('extension.list.profile')->getPath('kwall_edu') . '/content-export/' . $entityType . '.json');
    $data = json_decode($json, TRUE);

    $queue = \Drupal::queue('kwall_edu_import_' . $entityType);
    $queue->createQueue();

    foreach ($data as $item) {
      $type = $item["type"][0]["target_id"];
      $values = [];
      $fields = $entityFieldTypes[$type];
      foreach ($fields as $fieldName => $fieldType) {
        if (!empty($item[$fieldName])) {
          switch ($fieldType) {

            case 'entity_reference':
            case 'file':
            case 'svg_image_field':
              foreach ($item[$fieldName] as $target) {
                $values[$fieldName][] = [
                  'target_id' => $target['target_id']
                ];
              }
              break;

            case 'entity_reference_revisions':
              foreach ($item[$fieldName] as $target) {
                $values[$fieldName][] = [
                  'target_id' => $target['target_id'],
                  'target_revision_id' => $target['target_revision_id'],
                ];
              }
              break;

            default:
              switch ($fieldName) {
                case 'field_post_date':
                case 'field_date_range':
                case 'field_start_date':
                case 'field_end_date':
                case 'field_event_date':
                case 'field_event_dates':
                case 'field_date_recurring':
                case 'field_expiration_date_alert':
                  foreach ($item[$fieldName] as &$target) {
                    $UTC_offset = substr($target['value'], -6);
                    $timezone = new DateTimeZone(str_replace(':', '', $UTC_offset));
                    $date = DrupalDateTime::createFromFormat('Y-m-d\TH:i:sP', $target['value'], $timezone);
                    $date->setTimezone(new Datetimezone('UTC'));
                    $target["value"] = $date->format("Y-m-d\TH:i:s");
                    if (!empty($target["end_value"])) {
                      $date = DrupalDateTime::createFromFormat('Y-m-d\TH:i:sP', $target['end_value'], $timezone);
                      $date->setTimezone(new Datetimezone('UTC'));
                      $target["end_value"] = $date->format("Y-m-d\TH:i:s");
                    }
                  }
                  break;

                case 'field_icon':
                  foreach ($item[$fieldName] as &$target) {
                    unset($target["settings"]);
                  }
                  break;
              }
              $values[$fieldName] = $item[$fieldName];
              break;
          }
        }

      }

      if ($entityType == 'node') {
        $values += [
          'nid' => $item["nid"][0]["value"],
          'uuid' => $item["uuid"][0]["value"],
          'title' => $item["title"][0]["value"],
          'moderation_state' => 'published',
          'type' => $type,
          'status' => $item["status"][0]["value"]
        ];
      } else if ($entityType == 'paragraph') {
        $values += [
          'id' => $item["id"][0]["value"],
          'uuid' => $item["uuid"][0]["value"],
          'revision_id' => $item["revision_id"][0]["value"],
          'type' => $type,
          'status' => $item["status"][0]["value"],
          'parent_id' => $item["parent_id"][0]["value"],
          'parent_type' => $item["parent_type"][0]["value"],
          'parent_field_name' => $item["parent_field_name"][0]["value"],
        ];
      } else if ($entityType == 'block_content') {
        $values += [
          'id' => $item["id"][0]["value"],
          'uuid' => $item["uuid"][0]["value"],
          'info' => $item["info"][0]["value"],
          'type' => $type,
          'status' => $item["status"][0]["value"],
          'reusable' => $item["reusable"][0]["value"],
        ];
      }
      $queue->createItem($values);
    }

  }

  /**
   * Imports menu links.
   */
  public function importMenuLinks() {
    $json = file_get_contents(\Drupal::service('extension.list.profile')->getPath('kwall_edu') . '/content-export/menu.json');
    $data = json_decode($json, TRUE);

    $queue = \Drupal::queue('kwall_edu_import_menu_link_content');
    $queue->createQueue();

    foreach ($data as $item) {
      $menu = $item["menu_name"][0]["value"];
      if ($menu != 'admin' && $menu != 'devel' && $menu != 'tools' && $menu != 'account') {
        $queue->createItem($item);
      }
    }
  }

  /**
   * Get entity field types.
   */
  protected function getEntityFieldDefinition($entityType) {

    $defaultFields = $this->getEntityFieldDefinitionDefault($entityType);

    $entityFieldTypes = [];
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entityType);
    foreach ($bundles as $name => $bundle) {
      $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entityType, $name);
      $diffFields = array_diff_assoc(array_flip(array_keys($definitions)), array_flip($defaultFields));
      $fieldTypes = [];
      foreach ($diffFields as $fieldName => $i) {
        $fieldTypes[$fieldName] = $definitions[$fieldName]->getType();
      }
      $entityFieldTypes[$name] = $fieldTypes;
    }

    return $entityFieldTypes;
  }

  /**
   * Get default entity fields.
   */
  protected function getEntityFieldDefinitionDefault($entityType) {
    switch ($entityType) {
      case 'node':
        $defaultFields = [
          'nid',
          'uuid',
          'vid',
          'langcode',
          'type',
          'revision_timestamp',
          'revision_uid',
          'revision_log',
          'status',
          'uid',
          'title',
          'created',
          'changed',
          'promote',
          'sticky',
          'default_langcode',
          'revision_default',
          'revision_translation_affected',
          'moderation_state',
          'metatag',
          'path',
          'publish_on',
          'unpublish_on',
          'menu_link',
          'field_meta_tags'
        ];
        break;

      case 'paragraph':
        $defaultFields = [
          'id',
          'uuid',
          'revision_id',
          'langcode',
          'type',
          'status',
          'created',
          'parent_id',
          'parent_type',
          'parent_field_name',
          'behavior_settings',
          'default_langcode',
          'revision_default',
          'revision_translation_affected',
        ];
        break;

      case 'block_content':
        $defaultFields = [
          'id',
          'uuid',
          'revision_id',
          'langcode',
          'type',
          'revision_created',
          'revision_user',
          'revision_log',
          'status',
          'info',
          'changed',
          'reusable',
          'created',
          'default_langcode',
          'revision_translation_affected',
          'metatag'
        ];
        break;

      default:
        $defaultFields = [];
        break;
    }

    return $defaultFields;
  }

}